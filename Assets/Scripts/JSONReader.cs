using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JSONReader : MonoBehaviour
{
    public TextAsset JSONText;



    [System.Serializable]
    public class Assets
    {
        public string assetName;
        public string downloadUrl;

    }

    [System.Serializable]
    public class AssetList
    {
        public Assets[] assets;
    }

    public AssetList myAssetList = new AssetList(); 
    void Start()
    {
        myAssetList = JsonUtility.FromJson<AssetList>(JSONText.text);
        Debug.Log(myAssetList);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
