using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

using Firebase;
using Firebase.Extensions;
using Firebase.Storage;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;


public class PrefabLoader : MonoBehaviour
{
    RawImage rawImage;



    IEnumerator LoadImage(string MediaURL)
    {
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(MediaURL);
        yield return request.SendWebRequest();
        if(request.isNetworkError || request.isHttpError)
        {
            Debug.Log(request.error);
        }
        else
        {
            rawImage.texture = ((DownloadHandlerTexture)request.downloadHandler).texture;
        }
    }
    private void Start()
    {
        rawImage = gameObject.GetComponent<RawImage>();
        StartCoroutine(LoadImage("https://firebasestorage.googleapis.com/v0/b/roofstack-24061.appspot.com/o/uploads%2FLI%2BCOVER%2Bironhead_logo-art-1.png?alt=media&token=794ed97d-fbf4-440d-b5cd-8f56303e3e1a"));
    }


}
