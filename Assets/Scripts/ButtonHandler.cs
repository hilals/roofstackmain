using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

using Firebase;
using Firebase.Extensions;
using Firebase.Storage;



public class ButtonHandler : MonoBehaviour
{
    RawImage rawImage;
    //public GameObject cubeGO;
    public GameObject HomeButton;
    public GameObject CubeButton;
    public GameObject SphereButton;
    public GameObject CapsuleButton;

    IEnumerator LoadImage(string MediaURL)
    {
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(MediaURL);
        yield return request.SendWebRequest();
        if(request.isNetworkError || request.isHttpError)
        {
            Debug.Log(request.error);
        }
        else
        {
            rawImage.texture = ((DownloadHandlerTexture)request.downloadHandler).texture;
        }
    }

    private void Awake()
    {
        CubeButton.SetActive(false);
        SphereButton.SetActive(false);
        CapsuleButton.SetActive(false);
    }
  
    
    public void OnHomeButtonClicked()
    {
        HomeButton.SetActive(false);
        CubeButton.SetActive(true);
        SphereButton.SetActive(true);
        CapsuleButton.SetActive(true);
    }

    public void ChangeObjectTypeToCube()
    {
        rawImage = gameObject.GetComponent<RawImage>();
        StartCoroutine(LoadImage("https://drive.google.com/file/d/1yNsAnikgNQghMMVyEdDVGbUMJQZZfTmg/view?usp=sharing"));
    }

}
